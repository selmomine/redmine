FROM ruby:2.6
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client libpq-dev
RUN mkdir /redmine
WORKDIR /redmine
RUN gem install bundler

COPY config/database.yml /redmine/config/database.yml
COPY config/configuration.yml /redmine/config/configuration.yml
COPY config/additional_environment.rb /redmine/config/additional_environment.rb
COPY Gemfile /redmine/Gemfile
COPY Gemfile.lock /redmine/Gemfile.lock
RUN bundle install
RUN gem install tzinfo
COPY . /redmine

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]